<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/post/create', 'InformationController@store');
Route::get('/post/edit/{id}', 'InformationController@edit');
Route::post('/post/update/{id}', 'InformationController@update');
Route::delete('/post/delete/{id}', 'InformationController@delete');
Route::get('/posts', 'InformationController@index');