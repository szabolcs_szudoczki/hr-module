<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    protected $fillable = [
        'name', 
        'title', 
        'sex', 
        'birthday', 
        'position', 
        'department', 
        'office', 
        'workhours', 
        'email', 
        'address', 
        'mailaddress', 
        'phone', 
        'officephone', 
        'taxnumber', 
        'tajnumber', 
        'nationality', 
        'education', 
        'language', 
        'boss', 
        'job', 
        'status',
        'pname',
        'prelationship',
        'pphone',
        'sname',
        'srelationship',
        'sphone',
        'bankname',
        'bankaccountno'
    ];
}
