<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\InformationCollection;
use App\Information;

class InformationController extends Controller
{
    public function store(Request $request)
    {
      $this->validate($request, [
          'name' => 'required'
      ]);
      
      $information = new Information([
        'name' => $request->get('name'),
        'title' => $request->get('title'),
        'sex' => $request->get('sex'),
        'birthday' => $request->get('birthday'),
        'position' => $request->get('position'),
        'department' => $request->get('department'),
        'office' => $request->get('office'),
        'workhours' => $request->get('workhours'),
        'email' => $request->get('email'),
        'address' => $request->get('address'),
        'mailaddress' => $request->get('mailaddress'),
        'phone' => $request->get('phone'),
        'officephone' => $request->get('officephone'),
        'taxnumber' => $request->get('taxnumber'),
        'tajnumber' => $request->get('tajnumber'),
        'nationality' => $request->get('nationality'),
        'education' => $request->get('education'),
        'language' => $request->get('language'),
        'boss' => $request->get('boss'),
        'job' => $request->get('job'),
        'status' => $request->get('status'),
        'pname' => $request->get('pname'),
        'prelationship' => $request->get('prelationship'),
        'pphone' => $request->get('pphone'),
        'sname' => $request->get('sname'),
        'srelationship' => $request->get('srelationship'),
        'sphone' => $request->get('sphone'),
        'bankname' => $request->get('bankname'),
        'bankaccountno' => $request->get('bankaccountno')
      ]);

      $information->save();

      return response()->json('success');
    }

    public function index()
    {
        return new InformationCollection(Information::all());
    }

    public function edit($id)
    {
        $information = Information::find($id);
        return response()->json($information);
    }

    public function update($id, Request $request)
    {
        $information = Information::find($id);

        $information->update($request->all());

        return response()->json('successfully updated');
    }

    public function delete($id)
    {
        $information = Information::find($id);
        
        $information->delete();

        return response()->json('successfully deleted');
    }

    public function fileUpload(Request $request)
    {
      $fileName = time(). '_' . $request->file->getClientOriginalName() . '.' . $request->file->getClientOriginalExtension();
      $request->file->move(public_path('upload'), $fileName);

      return response()->json(['success'=>'You have successfully uploaded the file.']);
    }
}