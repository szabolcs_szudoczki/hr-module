<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('title')->nullable();
            $table->string('sex')->nullable();
            $table->date('birthday')->nullable();
            $table->string('position')->nullable();
            $table->string('department')->nullable();
            $table->string('office')->nullable();
            $table->integer('workhours')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('mailaddress')->nullable();
            $table->integer('phone')->nullable();
            $table->integer('officephone')->nullable();
            $table->integer('taxnumber')->nullable();
            $table->integer('tajnumber')->nullable();
            $table->string('nationality')->nullable();
            $table->string('education')->nullable();
            $table->string('language')->nullable();
            $table->string('boss')->nullable();
            $table->string('job')->nullable();
            $table->boolean('status')->nullable();
            $table->string('pname')->nullable();
            $table->string('prelationship')->nullable();
            $table->integer('pphone')->nullable();
            $table->string('sname')->nullable();
            $table->string('srelationship')->nullable();
            $table->integer('sphone')->nullable();
            $table->string('bankname')->nullable();
            $table->integer('bankaccountno')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information');
    }
}
